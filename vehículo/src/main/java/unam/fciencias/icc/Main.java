package unam.fciencias.icc;
import unam.fciencias.icc.entidades.*;
import unam.fciencias.icc.gui.*;

public class Main {
    public static void main(String[] args) {
        unam.fciencias.icc.InterfazGráfica ventana = new unam.fciencias.icc.InterfazGráfica();
        ventana.launchFrame();
    }
}
