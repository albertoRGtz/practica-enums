package unam.fciencias.icc.entidades;

public class Automovil extends VehículoTerrestre {
    private static final String modelo = "SEDÁN";

    public String getModelo() {
        return this.modelo;
    }

    public Automovil(){
        super.setMarcha(Marcha.P);
    }

    public Automovil(long kilometraje, double nivelCombustible, Marcha marcha) {
        this.setKilometraje(kilometraje);
        this.setNivelDeCombustible(nivelCombustible);
        this.setMarcha(marcha);
    }

    @Override
    public void bajarMarcha() {
        if(this.getMarcha() != Marcha.P) {
            switch (this.getMarcha()) {
                case R:
                    this.setMarcha(Marcha.P);
                    break;

                case N:
                    this.setMarcha(Marcha.R);
                    break;

                default:
                    this.setMarcha(Marcha.N);
                    break;
            }
        }
    }

    @Override
    public void subirMarcha() {
        if(this.getMarcha() != Marcha.D) {
            switch (this.getMarcha()) {
                case P:
                    this.setMarcha(Marcha.R);
                    break;

                case R:
                    this.setMarcha(Marcha.N);
                    break;

                default:
                    this.setMarcha(Marcha.D);
                    break;
            }
        }
    }

    @Override
    public String toString() {
        return "El automovil " + this.modelo + " tiene un kilometraje de " + this.getKilometraje() + ", "
                + this.getNivelDeCombustible() + " litros de combustible y está en" + this.getMarcha().getMarcha() + ".";
    }

    @Override
    public boolean equals(Object objeto) {
        boolean sonIguales = false;
        if (objeto instanceof Automovil) {
            Automovil automovil = (Automovil) objeto;
            return (this.getKilometraje() == automovil.getKilometraje()) && (this.getMarcha() == automovil.getMarcha())
                    && (this.getNivelDeCombustible() == automovil.getNivelDeCombustible());
        }
        return sonIguales;
    }
}
