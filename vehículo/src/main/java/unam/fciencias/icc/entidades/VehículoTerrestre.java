package unam.fciencias.icc.entidades;

import unam.fciencias.icc.Clima;

public abstract class VehículoTerrestre extends Vehículo {
    private Clima clima;

    public Clima getClima() {
        return clima;
    }

    public void setClima(Clima clima) {
        this.clima = clima;
    }
}
